from fastapi import APIRouter
from fastapi import Depends, Path, Query
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from utils.jwt_manager import create_token
from pydantic import BaseModel
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()


@movie_router.get('/movies', tags=['Movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())]) #Indica que va a devolver una lista
def get_movies() -> List[Movie]: #Indica que va a devolver una lista
  db = Session()
  result = MovieService(db).get_movies()
  return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/{id}', tags=['Movies'], response_model=Movie, status_code=404) #Poner parametro de ruta
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie: #Validar parametros de ruta
  db = Session()
  result = MovieService(db).get_movie(id)
  if not result:
    return JSONResponse(status_code=404, content={'message': "No Encontrado"})
  '''
  for item in movies:
    if item["id"] == id:
      return JSONResponse(content=item)
  '''
  return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/', tags=['Movies'], response_model=List[Movie]) #Con parametros query
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]: #Validar parametros query
  #data = [item for item in movies if item['category'] == category]
  db = Session()
  result = MovieService(db).get_movie_by_category(category)
  if not result:
    return JSONResponse(status_code=404, content={'message', "No encontrado"})
  return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code=201)
def create_movie(movie: Movie) -> dict:
  db = Session()
  MovieService(db).create_movie(movie)
  return JSONResponse(status_code=201, content={"message": "Se ha registrado la película"})

@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
  db = Session()
  result = MovieService(db).get_movie(id)
  if not result:
    return JSONResponse(status_code=404, content={'message', "No encontrado"})
  
  MovieService(db).update_movie(id, movie)
  return JSONResponse(status_code=200, content={"message": "Se ha modificado la película"})
    
@movie_router.delete('/movies/{id}' , tags=['movies'], response_model=dict, status_code=200)
def delete_movie(id: int) -> dict:
  db = Session()
  result: MovieModel = db.query(MovieModel).filter(MovieModel.id == id).first()
  if not result:
    return JSONResponse(status_code=404, content={'message', "No encontrado"})
  MovieService(db).delete_movie(id)
  return JSONResponse(status_code=200, content={"message": "Se ha eliminado la película"})
  