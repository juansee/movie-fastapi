import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base #Manipular tablas

sqlite_file_name = "../database.sqlite"
base_dir = os.path.dirname(os.path.realpath(__file__)) #leer directorio actual de database.py

database_url = f"sqlite:///{os.path.join(base_dir, sqlite_file_name)}" #Join une las 2 URLs

engine = create_engine(database_url, echo=True) #Motor de la base de datos

Session = sessionmaker(bind=engine)

Base = declarative_base()
