from pydantic import BaseModel, Field
from typing import Optional, List

class Movie(BaseModel):
  id: Optional[int] = None #Tipo Opcional
  #title: str = Field(default="Mi película", min_length=5, max_length=15) Asignar valor por default o como esta abajo
  title: str = Field(min_length=5, max_length=15)
  overview: str = Field(dmin_length=15, max_length=100)
  year: int = Field(le=2022)
  rating: float = Field(ge=1, le=10)
  category: str = Field(max_length=15)

  class Config:
    schema_extra = {   #Valores por defecto
      "example": {
        "id": 1,
        "title": "Mi película",
        "overview": "Descripción de la película",
        "year": 2022,
        "rating": 9.8,
        "category": "Acción"
      }
    }